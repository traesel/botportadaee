/*
Arquivo de respostas programadas do bot
Ao editar este arquivo, inclua seu nome no atributo "Devs"
O atributo "Adms" contém a lista de usuários pré autorizados a conversar com o bot
*/

const char * Version = "V 1.1";

const int SZ = 12; //Tamanho da string para este compialdor

String senha= "DU00FAVIDAS?";

const String comandos2[]= {
	"ABRIR",
	"LIST",
	"HELP",
	"DEVELOPERS"
};

const String Contato = "537826716";
const String Adms[] = {
//	"537826716",
	"-384817062",
	"-1001352248744"
};

const String Devs[] = {
	"As pessoas envolvidas na minha cognição são:",
	"1Berto Kramm",
	"Cassiano Campes",
	"Edson Traesel",
	Version
};



const String despertar[]= {
	"I'm Alive!",
	"é nóis!! tô de volta",
	"Não sei quem foi o vagabundo que me desligou... mas já tô de volta",
	"Beijo no ombro pras inimigas que tramaram minha queda",
	"tô de volta!!! Vocês vão ter que me engolir!",
	"Será possivel que vão ficar me desligando toda hora?"
};



const String helpMenu[]= {
	"por enquanto as únicas palavras que eu entendo são:",
	"`abrir` - para abrir a porta,",
	"`list` - para ver os usuários autorizados,",
	"`developers` - para ver os usuários que editaram este arquivo e",
	"`help` - para ver o menu de ajuda",
	"Além do mais, pelo que me consta... nem estou ligado na porta ainda"
};


const String keyboardJson[] = {
	"[[{ \"text\" : \"Abrir Porta\", \"callback_data\" : \"/abrir\" }]",
//	",[{ \"text\" : \"Ver uruários autenticados\", \"callback_data\" : \"/list\" }]",
	",[{ \"text\" : \"Desenvolvedores\", \"callback_data\" : \"/developers\" }]",
	",[{ \"text\" : \"Drive da Elétrica\", \"url\" : \"http://bit.ly/2KBMSS7\" }]",
	",[{ \"text\" : \"Documentação\", \"url\" : \"https://gitlab.com/humbertokramm/botportadaee\" }]",
	",[{ \"text\" : \"sobre o DAEE\", \"url\" : \"https://daeeunisinos.home.blog\" }]",
	"]"
};


const String desconhecido[]= {
	"acho que não conheço vc, ou talvez tenha me esquecido, mas se vc disser a palavra mágica talvez eu possa te ajudar",
	"vai te deitar rapaz, nem te conheço",
	"\xF0\x9F\x98\x92",	//😒 UNAMUSED FACE
	"\xF0\x9F\x98\xA4",	//😤 FACE WITH LOOK OF TRIUMPH
	"\xF0\x9F\x98\x9D" 	//😝 FACE WITH STUCK-OUT TONGUE AND TIGHTLY-CLOSED EYES
};

const String cadastrado[]= {
	"Aeeeee",
	"Bora mandar uns comandos agora",
	"\xF0\x9F\x98\x81"	//😁 GRINNING FACE WITH SMILING EYES
};

const String erro[]= {
	"comando invalido",
	"não tô entendendo o que vc está falando. Digita: help, para ver as opções",
	"q c tá falando cara?",
	"seu palestrinha, eu só entendo alguns comandos",
	"tenta o comando: help",
	"não sei se eu consigo te ajudar assim",
	"eu não falar seu língua"
	"mas tchê, vai te deitar e me deixa",
	"guri para de xineliar e vai trabalhar",
	"\xF0\x9F\x98\x92",	//😒 UNAMUSED FACE
	"\xF0\x9F\x98\xA4",	//😤 FACE WITH LOOK OF TRIUMPH
	"\xF0\x9F\x98\x9D"	//😝 FACE WITH STUCK-OUT TONGUE AND TIGHTLY-CLOSED EYES

};


const String portaAberta[]= {
	"porta liberada! #sqn",
	"porta destrancada!",
	"acesso liberado",
	"vai firme",
	"feeeito!",
	"\xF0\x9F\x91\x8D",	//👍 THUMBS UP SIGN
	"\xF0\x9F\x98\x89"	//😉 WINKING FACE
};

const String barWords[]= {
	"CERVA",
	"CERVEJA",
	"BAR",
	"TOMAR UMA"
};


const String barMsgs[]= {
	"partiu Alemão",
	"deixa que eu pago",
	"saidera",
	"quem leva a carne?",
	"\xF0\x9F\x8D\xBA",	//🍺 BEER MUG
	"\xF0\x9F\x98\x8D",	//😍 SMILING FACE WITH HEART-SHAPED EYES
	"\xF0\x9F\x98\x8B"	//😋 FACE SAVOURING DELICIOUS FOOD
};


const String badWords[]= {
	"PORRA",
	"FODA",
	"CARALHO",
	"FUDER"
};

const String badMsgs[]= {
	"que boca suja jovem?!",
	"modere seu linguajar",
	"\xF0\x9F\x98\xB1",//😱 FACE SCREAMING IN FEAR
	"vou lavar sua boca com sabão"
};

const String alert1[]= {
	"verifique a porta, pois ela ficou aberta",
	"mantenha a porta fechada",
	"não deixe a porta aberta",
	"a porta ficou aberta"
};

const String alert2[]= {
	"já tem muito tempo que a porta está aberta",
	"por favor, mantenha a porta fechada",
	"quanto tempo precisa para passar por essa porta?",
	"deixou o rabo na porta?"
};

const String alert3[]= {
	"já tem 2 minutos que essa porta está aberta \xF0\x9F\x98\xA1",//😡 POUTING FACE
	"2 minutos, não deu para passar ainda? \xF0\x9F\x98\xA1",//😡 POUTING FACE
	"meu deus, fecha essa porta! \xF0\x9F\x98\xA1",//😡 POUTING FACE
	"qual é a dificuldade de fechar uma porta? \xF0\x9F\x98\xA1",//😡 POUTING FACE
};

const String lantinBasicChar[]={
	"À","Á","Â","Ã","Ä","Å","Æ","Ç","È","É","Ê","Ë","Ì","Í","Î","Ï",	//U00C0 até U00CF
	"Ð","Ñ","Ò","Ó","Ô","Õ","Ö","×","Ø","Ù","Ú","Û","Ü","Ý","Þ","ß",	//U00D0 até U00DF
	"à","á","â","ã","ä","å","æ","ç","è","é","ê","ë","ì","í","î","ï",	//U00E0 até U00EF
	"ð","ñ","ò","ó","ô","õ","ö","÷","ø","ù","ú","û","ü","ý","þ","ÿ" 	//U00F0 até U00FF
};

